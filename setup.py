from setuptools import setup, find_packages


setup(
    name="lemony.kibana",
    version="0.0.4",
    description="An interface to undocumented Kibana HTTP API",
    author="Cedric Nelson",
    author_email="cedricnelson@gmail.com",
    url="https://gitlab.com/ced/lemony_kibana",
    keywords="kibana elasticsearch lemony",
    packages=find_packages(exclude=["docs", "tests"]),
    namespace_packages=["lemony"],
    scripts=['scripts/test_index_pattern_creation.py'],
    install_requires=[
        "requests==2.19.1",
    ],
    test_suite="tests",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
    ],
)
