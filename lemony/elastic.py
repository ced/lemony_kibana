"""
A thin wrapper around elasticsearch module
"""

from deepdiff import DeepDiff
import elasticsearch
from elasticsearch.helpers import bulk


class Elastic:
    "A thin wrapper around elasticsearch module"

    def __init__(self, host="localhost", port=9200):
        self.es = elasticsearch.Elasticsearch([{"host": host, "port": port}])
        self.ic = elasticsearch.client.IndicesClient(self.es)
        self.cat = elasticsearch.client.CatClient(self.es)

    def bulk_ingest(self, name, records):
        "Ingest data into ElasticSearch"
        (success, _) = bulk(self.es, records, index=name)
        return success

    def create_index(self, name, doc_type, mapping):
        "Create an index"
        if name not in self.indices():
            self.ic.create(index=name)
            self.ic.put_mapping(index=name, doc_type=doc_type, body=mapping)
            return

        current_mappings = self.ic.get_mapping(index=name)
        if doc_type not in current_mappings[name]["mappings"]:
            self.ic.put_mapping(index=name, doc_type=doc_type, body=mapping)
            return

        if DeepDiff(mapping, current_mappings[name]["mappings"][doc_type], ignore_order=True):
            self.ic.put_mapping(index=name, doc_type=doc_type, body=mapping)

    def delete_index(self, name):
        "Delete an index"
        return self.ic.delete(index=[name])

    def indices(self, params={"format": "json", "h": ["index"]}):
        "Return a list of indices"
        return [i["index"] for i in self.cat.indices(**params)]

    def index_exists(self, name):
        "Return True if the index exists"
        return name in self.indices()
