"""
An interface to undocumented Kibana HTTP API
"""

import json
import random
import urllib.parse

import requests


class Kibana:
    "Provide an interface to Kibana"

    def __init__(self, host="localhost", port=5601, scheme="http", api_path="api/", kbn_version="6.3.2"):
        self.base_url = "{scheme}://{host}:{port}".format(scheme=scheme, host=host, port=port)
        self.api_url = "{base_url}/{path}".format(base_url=self.base_url, path=api_path)
        self.session = requests.Session()
        self.session.headers.update({"kbn-version": kbn_version})

    def _delete(self, url):
        "Abstraction for DELETE requests"
        resp = self.session.delete(url)
        resp.raise_for_status()
        return resp.json()

    def _get(self, url, params={}):
        "Abstraction for GET requests"
        resp = self.session.get(url, params=params)
        resp.raise_for_status()
        return resp.json()

    def _post(self, url, payload={}, params={}):
        "Abstraction for POST requests"
        resp = self.session.post(url, params=params, json=payload)
        resp.raise_for_status()
        return resp.json()

    def _put(self, url, payload={}, params={}):
        "Abstraction for PUT requests"
        resp = self.session.put(url, params=params, json=payload)
        resp.raise_for_status()
        return resp.json()

    def _paginate(self, url, collect_on, params={}, per_page=1000):
        "Pagination support for GET requests"

        data = {"page": 0}
        collected = []

        while True:
            data = self._get(url, params={**{"per_page": per_page},
                                          **params,
                                          **{"page": data["page"] + 1}})
            if not data[collect_on]:
                break
            collected.extend(data[collect_on])
        return {**data, **{collect_on: collected}}

    def bulk_get_saved_objects(self, payload):
        "Get lots of saved objects"
        url = urllib.parse.urljoin(self.api_url, "saved_objects/_bulk_get")
        return self._post(url, payload)

    def change_settings(self, settings):
        "Change the Kibana settings"
        url = urllib.parse.urljoin(self.api_url, "kibana/settings")
        return self._post(url, {"changes": settings})

    def create_index_pattern(self, title, timeFieldName, custom_id=None, set_as_default=False):
        "Create an index pattern"

        if title in self.index_pattern_titles():
            return

        if custom_id:
            url = urllib.parse.urljoin(self.api_url, "saved_objects/index-pattern/" + custom_id)
        else:
            url = urllib.parse.urljoin(self.api_url, "saved_objects/index-pattern")

        payload = {
            "attributes": {
                "title": title,
                "timeFieldName": timeFieldName
            }
        }

        resp = self._post(url, payload)

        if set_as_default or "defaultIndex" not in self.settings():
            self.change_settings({"defaultIndex": resp["id"]})

        # Set fields for index pattern
        fields = self.index_pattern_fields_for_wildcard(title)
        self.update_index_pattern(
            resp["id"],
            {"attributes": {**resp["attributes"],
                            "fields": json.dumps(fields["fields"])}})

        return resp

    def delete_index_pattern(self, id):
        "Delete an index pattern"
        url = urllib.parse.urljoin(self.api_url, "saved_objects/index-pattern/{id}".format(id=id))
        search_config = {"type": "index-pattern", "fields": "title"}

        current_settings = self.settings()
        if "defaultIndex" in current_settings and id == current_settings["defaultIndex"]["userValue"]:
            # Set another index as the default
            other_index_patterns = [p
                                    for p in self.find_saved_objects(**search_config)["saved_objects"]
                                    if p["id"] != id]
            if other_index_patterns:
                new_default_id = random.choice(other_index_patterns)["id"]
            else:
                new_default_id = None
            self.change_settings({"defaultIndex": new_default_id})

        return self._delete(url)

    def elasticsearch(self, search={}, indices="*"):
        "Return results of an elasticsearch query proxied through kibana"
        url = urllib.parse.urljoin(self.base_url, "elasticsearch/{indices}/_search".format(indices=indices))
        default_search = {"size": 0, "aggs": {"indices": {"terms": {"field": "_index", "size": 20000}}}}
        if search:
            payload = search
        else:
            payload = default_search
        return self._post(url, payload)

    def find_saved_objects(self, **params):
        "Return saved objects that match the search criteria"
        url = urllib.parse.urljoin(self.api_url, "saved_objects/_find")
        return self._paginate(url, "saved_objects", params)

    def import_saved_object(self, saved_object, overwrite=False, params={}):
        "Import the saved object to Kibana"
        url = urllib.parse.urljoin(self.api_url, "saved_objects/{obj_type}/{obj_id}".format(
            obj_type=saved_object["_type"],
            obj_id=saved_object["_id"]))
        return self._post(url,
                          {"attributes": saved_object["_source"]},
                          params={**{"overwrite": str(overwrite).lower()}, **params})

    def index_pattern_exists(self, title):
        "Return True if the index pattern exists"
        return title in self.index_pattern_titles()

    def index_pattern_fields_for_wildcard(self, pattern):
        "Get a list of fields for an index pattern"
        url = urllib.parse.urljoin(self.api_url, "index_patterns/_fields_for_wildcard")
        params = {
            "pattern": pattern,
            "meta_fields": ["_source", "_id", "_type", "_index", "_score"]
        }
        return self._get(url, params)

    def index_pattern_titles(self, params={"type": "index-pattern", "fields": "title"}):
        "Return a list of index patterns"
        return [ip["attributes"]["title"] for ip in self.find_saved_objects(**params)["saved_objects"]]

    def index_patterns(self, id):
        "Return a index patterns matching the id"
        return self.bulk_get_saved_objects([{"id": id, "type": "index-pattern"}])

    def settings(self):
        "Return kibana settings"
        url = urllib.parse.urljoin(self.api_url, "kibana/settings")
        return self._get(url)

    def update_index_pattern(self, id, payload):
        "Issue a PUT against an index pattern"
        url = urllib.parse.urljoin(self.api_url, "saved_objects/index-pattern/{id}".format(id=id))
        return self._put(url, payload)
