# Summary

This module provides interface to undocumented Kibana HTTP API. I'm currently using it to help automate kibana configuration with puppet ^_^

# A quick check of functionality

```
python3 setup.py sdist bdist_wheel
pipenv --python 3.5
pipenv install dist/lemony.kibana-0.0.4-py2.py3-none-any.whl
pipenv install -r requirements-dev.txt
docker-compose up -d
pipenv run python ./scripts/test_index_pattern_creation.py
```
Then navigate to http://localhost:5601/app/kibana#/management/kibana/index?_g=()

# Clean-up

```
docker-compose down
```