"""
Create an elasticsearch index, populate it with some records, and
create a kibana index pattern for it.
"""

from datetime import datetime, timedelta
import random
import string

from lemony.elastic import Elastic
from lemony.kibana import Kibana


doc_type = "doc"

lemon_mapping = {
    "properties": {
        "lemon_count": {
            "type": "long"
        },
        "@timestamp": {
            "type": "date",
            "format": "epoch_second||epoch_millis"
        }
    }
}


def gen_records(doc_type, mapping, max_records=50):
    "Return a random number of ElasticSearch records"
    numeric_types = ("long", "integer", "short", "byte", "double", "float", "half_float", "scaled_float")
    floaty_types = ("float", "half_float", "scaled_float")
    texty_types = ("text", "keyword")
    numeric_min = 1
    numeric_max = 500

    now = datetime.now()
    records = []
    sort_by = None
    for _ in range(random.randint(1, max_records)):
        record = {}
        for (key, value) in mapping["properties"].items():
            rando = random.randint(numeric_min, numeric_max)
            if value["type"] in floaty_types:
                record[key] = rando
            elif value["type"] in numeric_types:
                record[key] = rando
            elif value["type"] in texty_types:
                record[key] = random_string(rando)
            elif value["type"] == "date":
                sort_by = key
                dt = now - timedelta(seconds=rando)
                record[key] = int(dt.strftime("%s"))
        if "_type" not in record:
            record["_type"] = doc_type
        records.append(record)

    if not sort_by:
        return records
    return sorted(records, key=lambda x: x[sort_by])


def get_time_field_name(mapping):
    "Return a time field name from the mapping"
    for (key, value) in mapping["properties"].items():
        if value["type"] == "date":
            return key

    raise RuntimeError("No time field found!")


def random_string(length=13):
    "Return a random string of given length"
    random.seed()
    return "".join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])


if __name__ == "__main__":
    target_index = "{name}-{date}".format(
        name="lemony", date=datetime.now().strftime("%Y.%m.%d"))

    ela = Elastic()
    ela.create_index(target_index, doc_type, lemon_mapping)
    ela.bulk_ingest(target_index, gen_records(doc_type, lemon_mapping))

    kibs = Kibana()
    kibs.create_index_pattern(title="lemony-*",
                              timeFieldName=get_time_field_name(lemon_mapping))
