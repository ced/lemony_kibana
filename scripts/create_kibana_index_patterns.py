#!/usr/bin/env python3
"""
Automatically create Kibana index patterns for
ES indices named:
<index_name>-<yyyy>.<mm>.<dd>
with a timeFieldName set to @timestamp
"""

import argparse
import re

from lemony.kibana import Kibana

INDEX_REGEX = re.compile(r"^(?P<index>\S+?)(?:-(?P<date>(?P<year>\d{4})\.(?P<month>\d{2})\.(?P<day>\d{2})))$")
TIME_FIELD_NAME = "@timestamp"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create Kibana index-patterns")
    parser.add_argument(
        "--host",
        default="localhost",
        help="Kibana host")
    parser.add_argument(
        "--port",
        type=int,
        default=5601,
        help="Kibana port")

    kwargs = vars(parser.parse_args())

    kibs = Kibana(host=kwargs["host"], port=kwargs["port"])
    es_result = kibs.elasticsearch()
    es_indices = [b["key"] for b in es_result["aggregations"]["indices"]["buckets"]]

    # Create needed index patterns
    for es_index in es_indices:
        match = INDEX_REGEX.match(es_index)
        if not match:
            print("Skipping es index ({name}). It doesn't match our index-pattern regex".format(
                name=es_index))
            continue
        match_dict = match.groupdict()
        index_pattern = "{index}-*".format(index=match_dict["index"])

        print("Looking for index {es_index}".format(es_index=es_index))
        if kibs.index_pattern_exists(index_pattern):
            print("Index pattern {pattern} already exists".format(
                pattern=index_pattern))
            continue

        kibs.create_index_pattern(title=index_pattern,
                                  timeFieldName=TIME_FIELD_NAME)
        print("Created index pattern {pattern}".format(pattern=index_pattern))

